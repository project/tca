# TOKEN CONTENT ACCESS

Token Content Access allows you to restrict access to individual nodes using
URL tokens. In order to view protected nodes, users must provide a unique
token via the URL. This allows nodes to be published and viewable
to anonymous users (for instance with a special link from an email campaign)
but not visible to the public at large. It also automatically hides any
TCA-protected content from Views results.

The module has API to open this ability to other entities such as
taxonomy terms, users, files and others. See tca_node module.

  * For a full description of the module visit:
    <https://www.drupal.org/project/tca>

  * To submit bug reports and feature suggestions, or to track changes visit:
    <https://www.drupal.org/project/issues/tca>


## REQUIREMENTS

This module requires no modules outside of Drupal core.
Additionally could be installed clipboardjs module to use clipboard features:
<https://www.drupal.org/project/clipboardjs>


## INSTALLATION

  * Install the Token Content Access module as you would normally install a
    contributed Drupal module. Visit <https://www.drupal.org/node/1897420> for
    further information.
  * Enable the Token Content Access nodes if you want to use token access
    for nodes.


## CONFIGURATION

  1. Navigate to Administration > Extend and enable the module.
  2. Navigate to Administration > Structure > Content types. Configure per
     Content Type (e.g.: /admin/structure/types/manage/article), editing
     token access on the Token Content Access settings tab. Save.
  3. Navigate to Content link. Configure per Node entity, editing token
     access on the Token Content Access settings tab. Save.


## MAINTAINERS

- Cyril Loboda (lobodacyril) - <https://www.drupal.org/u/lobodacyril>
