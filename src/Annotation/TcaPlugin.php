<?php

namespace Drupal\tca\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a TCA plugin item annotation object.
 *
 * @see \Drupal\tca\Plugin\TcaPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class TcaPlugin extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The string id of the affected entity.
   *
   * @var string
   */
  public $entityType;

}
