<?php

namespace Drupal\tca\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\tca\Plugin\TcaPluginManager;
use Drupal\tca\TcaSettingsManager;

/**
 * Token Content Access access check.
 */
class TcaAccessCheck implements AccessInterface {

  /**
   * The entity type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager = NULL;

  /**
   * The tca plugin manager.
   *
   * @var Drupal\tca\Plugin\TcaPluginManager
   */
  protected $tcaPluginManager = NULL;

  /**
   * The tca settings manager.
   *
   * @var Drupal\tca\TcaSettingsManager
   */
  protected $tcaSettingsManager = NULL;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a new \Drupal\tca\Access\TcaAccessCheck object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\tca\Plugin\TcaPluginManager $tca_plugin_manager
   *   The tca plugin manager.
   * @param \Drupal\tca\TcaSettingsManager $tca_settings_manager
   *   The tca settings manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TcaPluginManager $tca_plugin_manager, TcaSettingsManager $tca_settings_manager, AccountInterface $current_user) {
    $this->entityTypeManager = $entity_type_manager;
    $this->tcaPluginManager = $tca_plugin_manager;
    $this->tcaSettingsManager = $tca_settings_manager;
    $this->currentUser = $current_user;
  }

  /**
   * Checks access to the node add page for the node type.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $user_token
   *   The TCA token.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   (optional) The account - if not provided will use the current user.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   A \Drupal\Core\Access\AccessInterface value.
   */
  public function access(EntityInterface $entity, $user_token, ?AccountInterface $account = NULL) {
    $neutral = AccessResult::neutral()
      ->addCacheableDependency($entity)
      ->addCacheContexts(['url.path']);

    $entity_type_id = $entity->getEntityTypeId();
    $entity_id = $entity->id();
    $affected_types = $this->tcaPluginManager->loadSupportedEntityTypes();
    $affected_bundle_types = $this->tcaPluginManager->loadSupportedBundleEntityTypes();

    if (!$account) {
      $account = $this->currentUser;
    }
    $bypass_permitted = $account->hasPermission('tca bypass ' . $entity_type_id);

    // If user has bypass permission or entity is not alowed for TCA, exit.
    if ($bypass_permitted || (!in_array($entity_type_id, $affected_types) &&
        !in_array($entity_type_id, $affected_bundle_types))) {
      return $neutral;
    }

    $entity_type = $this->entityTypeManager->getStorage($entity_type_id)
      ->getEntityType();

    // TRUE if an entity such as node_type.
    $is_entity_bundle = $this->isEntityBundle($entity);

    $tca_bundle_settings = NULL;
    $tca_settings = NULL;
    $active = NULL;
    $token = NULL;

    // TCA for entity bundles such as node_type.
    if ($is_entity_bundle) {
      // Load TCA settings for entity.
      $tca_settings = $this->tcaSettingsManager
        ->loadSettingsAsConfig($entity_type_id, $entity_id);

      $active = $tca_settings->get('active');
      $token = $tca_settings->get('token');
      $public = $tca_settings->get('public');
    }
    // TCA for end entities such as node.
    else {
      $bundle_entity_type_id = $entity_type->getBundleEntityType()
        ?: $entity_type_id;
      $bundle_entity_id = $entity->getEntityType()->getBundleEntityType()
        ? $entity->bundle() : NULL;

      // Load TCA settings for entity bundle.
      $tca_bundle_settings = $this->tcaSettingsManager
        ->loadSettingsAsConfig($bundle_entity_type_id, $bundle_entity_id);

      // If the form is about to be attached to an entity,
      // but the bundle isn't allowed to be overridden, exit.
      if (!$tca_bundle_settings->get('active')) {
        return $neutral;
      }

      // Load TCA settings for entity.
      $tca_settings = $this->tcaSettingsManager
        ->loadSettingsAsConfig($entity_type_id, $entity_id);

      $active = $tca_settings->get('active');
      $token = $tca_settings->get('token');
      $public = $tca_settings->get('public');
    }

    // If TCA is not active, exit.
    if (!$active) {
      return $neutral;
    }

    // If an entity has TCA enabled and token doesnt match up, then explicitly
    // deny access.
    if (!$user_token || $token != $user_token) {
      return AccessResult::forbidden()
        ->addCacheableDependency($entity)
        ->addCacheContexts(['url.path']);
    }
    // If an entity has TCA enabled an set to public and token match up, and its
    // then explicitly allow access to bypass other permissions.
    elseif ($public && $token == $user_token) {
      return AccessResult::allowed()
        ->addCacheableDependency($entity)
        ->addCacheContexts(['url.path']);
    }

    return $neutral;
  }

  /**
   * Checks if the current entity is a bundle.
   */
  protected function isEntityBundle($entity) {
    return is_subclass_of($entity,
      'Drupal\Core\Config\Entity\ConfigEntityBundleBase');
  }

}
