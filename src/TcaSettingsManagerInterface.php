<?php

namespace Drupal\tca;

/**
 * Interface for Tca Settings Manager.
 *
 * @package Drupal\tca
 */
interface TcaSettingsManagerInterface {

  /**
   * Save settings for an entity or bundle.
   *
   * @param array $settings
   *   The settings for the TcaSettings entity.
   * @param string $entity_type_id
   *   The entity type (e.g. node) as a string.
   * @param string $entity_id
   *   The entity ID as a string.
   */
  public function saveSettings(array $settings, $entity_type_id, $entity_id);

  /**
   * Load settings for an entity or bundle, or load the defaults.
   *
   * Load TCA settings appropriate to the given config or
   * default settings if not available.
   *
   * @param string $entity_type_id
   *   The entity type (e.g. node) as a string.
   * @param string $entity_id
   *   The entity ID as a string.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The TcaSettings Config object.
   */
  public function loadSettingsAsConfig($entity_type_id, $entity_id);

}
