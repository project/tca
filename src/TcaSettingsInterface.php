<?php

namespace Drupal\tca;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Tca settings entities.
 */
interface TcaSettingsInterface extends ConfigEntityInterface {

  /**
   * Set the active value.
   *
   * @param bool $active
   *   The active to save. FALSE (Not active), TRUE (Active).
   */
  public function setActive($active);

  /**
   * Get the active value.
   *
   * @return bool
   *   The active value. FALSE (Not active), TRUE (Active).
   */
  public function getActive();

  /**
   * Set the token value.
   *
   * @param string $token
   *   The token is 64 length string.
   */
  public function setToken($token);

  /**
   * Get the token value.
   *
   * @return string
   *   64 length string.
   */
  public function getToken();

  /**
   * Get public indicator.
   *
   * Public content can be viewed by all users that have
   * token in the URL (regardless of permissions).
   *
   * @return bool
   *   TRUE if content is public, FALSE otherwise.
   */
  public function getPublic();

}
