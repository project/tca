<?php

namespace Drupal\tca;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\PrivateKey;
use Drupal\Core\Site\Settings;
use Drupal\tca\Entity\TcaSettings;

/**
 * Tca Settings Manager.
 *
 * @package Drupal\tca
 */
class TcaSettingsManager implements TcaSettingsManagerInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The private key service.
   *
   * @var \Drupal\Core\PrivateKey
   */
  protected $privateKey;

  /**
   * Constructs a new \Drupal\tca\TcaSettingsManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $etm
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Drupal\Core\PrivateKey $private_key
   *   The private key service.
   */
  public function __construct(EntityTypeManagerInterface $etm, ConfigFactory $config_factory, PrivateKey $private_key) {
    $this->entityTypeManager = $etm;
    $this->configFactory = $config_factory;
    $this->privateKey = $private_key;
  }

  /**
   * {@inheritdoc}
   */
  public function saveSettings(array $settings, $entity_type_id, $entity_id) {
    // Load real entity if it has settings.
    $entity = $this->getEntity($entity_type_id, $entity_id);
    if ($entity) {
      foreach ($settings as $key => $value) {
        $field = 'tca_' . $key;
        if ($entity->hasField($field)) {
          $entity->set($field, $value);
        }
      }
      $entity->save();
    }
    // Otherwise use config.
    else {
      $id = $this->generateSettingsFullId($entity_type_id, $entity_id);

      $entity = TcaSettings::load($id);
      if ($entity === NULL) {
        $entity_array = ['id' => $id];
        $entity_array += $settings;
        $entity = TcaSettings::create($entity_array);
      }
      else {
        foreach ($settings as $key => $setting) {
          $entity->set($key, $setting);
        }
      }
      $entity->set('entity_type_id', $entity_type_id);
      $entity->set('entity_id', $entity_id);
      $entity->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadSettingsAsConfig($entity_type_id, $entity_id) {
    // Load real entity if it has settings.
    $entity = $this->getEntity($entity_type_id, $entity_id);
    if ($entity) {
      $config = $this->configFactory->get('tca.tca_settings.default');
      $settings = $config->getRawData();
      foreach ($settings as $key => $value) {
        $field = 'tca_' . $key;
        if ($entity->hasField($field)) {
          if (isset($entity->get($field)->value)) {
            $settings[$key] = $entity->get($field)->value;
          }
        }
      }
      return $config->initWithData($settings);
    }
    // Otherwise use config.
    else {
      $id = $this->generateSettingsFullId($entity_type_id, $entity_id);

      $actual = $this->configFactory->get('tca.tca_settings.' . $id);
      if (!$actual->isNew()) {
        return $actual;
      }
      else {
        return $this->configFactory->get('tca.tca_settings.default');
      }
    }
  }

  /**
   * Get entity.
   *
   * @param string $entity_type_id
   *   The entity type (e.g. node) as a string.
   * @param string $entity_id
   *   The entity ID as a string.
   *
   * @return Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  private function getEntity($entity_type_id, $entity_id) {
    $entity_storage = $this->entityTypeManager->getStorage($entity_type_id);

    if ($entity_storage && $entity_id) {
      $entity = $entity_storage->load($entity_id);
      if ($entity && is_subclass_of($entity, 'Drupal\Core\Entity\ContentEntityBase')) {
        if ($entity->hasField('tca_active') && $entity->hasField('tca_token')) {
          return $entity;
        }
      }
    }

    return NULL;
  }

  /**
   * Generate a full ID based on entity type label, bundle label and entity id.
   *
   * @param string $entity_type_id
   *   The entity type (e.g. node) as a string.
   * @param string $entity_id
   *   The entity ID as a string.
   *
   * @return string
   *   The full id appropriate for a TcaSettings config entity.
   */
  private function generateSettingsFullId($entity_type_id, $entity_id) {
    return $entity_type_id . (!empty($entity_id) ? '_' . $entity_id : '');
  }

  /**
   * Generate a token value.
   *
   * @return string
   *   64 length string.
   */
  public function generateToken($entity_type_id, $entity_id) {
    $private_key = $this->privateKey->get();
    $hash_salt = Settings::getHashSalt();
    return Crypt::hashBase64($entity_type_id . $entity_id . microtime(), $private_key . $hash_salt);
  }

}
