<?php

namespace Drupal\tca;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\tca\Plugin\TcaPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Tca Permission Generator.
 *
 * @package Drupal\tca
 */
class TcaPermissionGenerator implements ContainerInjectionInterface {
  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManager
   */
  private $entityTypeManager = NULL;

  /**
   * The tca plugin manager.
   *
   * @var Drupal\tca\Plugin\TcaPluginManager
   */
  private $tcaPluginManager = NULL;

  /**
   * Constructs a new \Drupal\tca\TcaPermissionGenerator object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $etm
   *   The entity type manager.
   * @param \Drupal\tca\Plugin\TcaPluginManager $tca_plugin_manager
   *   The tca plugin manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   The string translation service.
   */
  public function __construct(EntityTypeManagerInterface $etm, TcaPluginManager $tca_plugin_manager, TranslationInterface $translation) {
    $this->entityTypeManager = $etm;
    $this->tcaPluginManager = $tca_plugin_manager;
    $this->stringTranslation = $translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.tca_plugin'),
      $container->get('string_translation')
    );
  }

  /**
   * Return an array of per-entity tca permissions.
   *
   * @return array
   *   An array of permissions
   */
  public function permissions() {
    $permissions = [];

    foreach ($this->tcaPluginManager->getDefinitions() as $def) {
      $entity_type = $this->entityTypeManager
        ->getStorage($def['entityType'])
        ->getEntityType();
      $permissions += [
        'tca administer ' . $def['entityType'] => [
          'title' => $this->t('Administer Token Content Access settings for %entity_type', [
            '%entity_type' => $entity_type->getLabel(),
          ]),
          'dependencies' => [
            'module' => [
              $def['provider'],
            ],
          ],
        ],
        'tca bypass ' . $def['entityType'] => [
          'title' => $this->t('Bypass Token Content Access action for %entity_type', [
            '%entity_type' => $entity_type->getLabel(),
          ]),
          'dependencies' => [
            'module' => [
              $def['provider'],
            ],
          ],
        ],
      ];
    }

    return $permissions;
  }

}
