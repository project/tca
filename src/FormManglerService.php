<?php

namespace Drupal\tca;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\tca\Plugin\TcaPluginManager;

/**
 * The form mangler service.
 *
 * @package Drupal\tca
 */
class FormManglerService {

  use StringTranslationTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager = NULL;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $bundleInfo = NULL;

  /**
   * The tca plugin manager.
   *
   * @var \Drupal\tca\Plugin\TcaPluginManager
   */
  protected $tcaPluginManager = NULL;

  /**
   * All bundle info.
   *
   * @var array
   */
  protected $allBundleInfo = [];

  /**
   * The tca settings manager.
   *
   * @var \Drupal\tca\TcaSettingsManager
   */
  protected $tcaSettingsManager = NULL;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new Drupal\tca\FormManglerService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $etm
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $etbi
   *   The entity type bundle info.
   * @param \Drupal\tca\Plugin\TcaPluginManager $tca_plugin_manager
   *   The tca plugin manager.
   * @param \Drupal\tca\TcaSettingsManager $tca_settings_manager
   *   The tca settings manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   The string translation service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(EntityTypeManagerInterface $etm, EntityTypeBundleInfo $etbi, TcaPluginManager $tca_plugin_manager, TcaSettingsManager $tca_settings_manager, TranslationInterface $translation, ModuleHandlerInterface $module_handler, MessengerInterface $messenger) {
    $this->entityTypeManager = $etm;
    $this->allBundleInfo = $etbi->getAllBundleInfo();
    $this->tcaPluginManager = $tca_plugin_manager;
    $this->tcaSettingsManager = $tca_settings_manager;
    $this->stringTranslation = $translation;
    $this->moduleHandler = $module_handler;
    $this->messenger = $messenger;
  }

  /**
   * Form structure for the TCA configuration.
   *
   * This should be used by other modules that wish to implement the TCA
   * configurations in any form.
   *
   * @param array $attach
   *   The form that the TCA form should be attached to.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that we're adding the form to, e.g. a node.  This should be
   *    defined even in the case of bundles since it is used to determine bundle
   *    and entity type.
   * @param string|int|object $form_state
   *   The form state.
   * @param string $form_id
   *   The form id.
   */
  public function addTcaSettingsToEntityForm(array &$attach, EntityInterface $entity, FormStateInterface $form_state, $form_id) {
    $this->addTcaSettingsToForm($attach, $entity->getEntityType()->id(),
      $entity, $form_state, $form_id);
  }

  /**
   * Common functionality for adding TCA options to forms.
   *
   * @param array $attach
   *   The form that the TCA form should be attached to.
   * @param string $entity_type_id
   *   The string ID of the entity type for the form, e.g. 'node'.
   * @param object $entity
   *   The entity that we're adding the form to, e.g. a node.  This should be
   *    defined even in the case of bundles since it is used to determine bundle
   *    and entity type.
   * @param string|int|object $form_state
   *   The form state.
   * @param string $form_id
   *   The form id.
   */
  private function addTcaSettingsToForm(
    array &$attach,
    $entity_type_id,
    $entity,
    FormStateInterface $form_state,
    $form_id,
  ) {

    $entity_type = $this->entityTypeManager->getStorage($entity_type_id)
      ->getEntityType();

    $is_entity_viewable = $this->entityTypeManager->getDefinition($entity_type_id)->hasViewBuilderClass();

    // TRUE if an entity such as node_type.
    $is_entity_bundle = $this->isEntityBundle($entity);

    $entity_id = $entity->id();
    $tca_bundle_settings = NULL;
    $tca_settings = NULL;
    $active = NULL;
    $token = NULL;

    // TCA for entity bundles such as node_type.
    if ($is_entity_bundle) {
      // Load TCA settings for entity.
      $tca_settings = $this->tcaSettingsManager
        ->loadSettingsAsConfig($entity_type_id, $entity_id);

      $active = $tca_settings->get('active');

      $forced = $tca_settings->get('force') ?? FALSE;
    }
    // TCA for end entities such as node.
    else {
      $bundle_entity_type_id = $entity_type->getBundleEntityType()
        ?: $entity_type_id;
      $bundle_entity_id = $entity->getEntityType()->getBundleEntityType()
        ? $entity->bundle() : NULL;

      // Load TCA settings for entity bundle.
      $tca_bundle_settings = $this->tcaSettingsManager
        ->loadSettingsAsConfig($bundle_entity_type_id, $bundle_entity_id);

      // If the form is about to be attached to an entity,
      // but the bundle isn't allowed to be overridden, exit.
      if (!$tca_bundle_settings->get('active')) {
        return;
      }

      // Load TCA settings for entity.
      $tca_settings = $this->tcaSettingsManager
        ->loadSettingsAsConfig($entity_type_id, $entity_id);

      $active = $tca_settings->get('active');

      $forced = $tca_bundle_settings->get('force') ?? FALSE;

      $token = $tca_settings->get('token');
    }

    $entity_plugin = $this->tcaPluginManager->createInstanceByEntityType(
      $is_entity_bundle && !empty($entity_type->getBundleOf())
        ? $entity_type->getBundleOf() : $entity_type_id);

    $form = [];
    // Wrap everything in a fieldset.
    $form['tca'] = [
      '#type' => 'details',
      '#title' => $this->t('Token Content Access settings'),
      '#open' => $active ? TRUE : FALSE,

      // @todo Should probably handle group in a plugin - not sure if, e.g.,
      // files will work in the same way and even if they do later entities
      // might not.
      '#group' => $is_entity_bundle ? 'additional_settings' : 'advanced',
      '#attributes' => ['class' => ['tca-settings-form']],
      '#tree' => FALSE,
    ];

    $form['tca']['tca_is_entity_bundle'] = [
      '#type' => 'value',
      '#value' => $is_entity_bundle,
    ];

    $form['tca']['tca_entity_type_id'] = [
      '#type' => 'value',
      '#value' => $entity_type_id,
    ];

    $form['tca']['tca_entity_id'] = [
      '#type' => 'value',
      '#value' => $entity_id,
    ];

    $form['tca']['tca_active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Token Content Access protection'),
      '#default_value' => ($forced) ?: $active,
      '#description' => $this->t('If this is checked, users with the Administer Token Content Access settings permission will be able to enable Token Content Access protection for individual entities.'),
    ];

    // Override setting if we're not editing a bundle and entity is viewable.
    if (!$is_entity_bundle && $is_entity_viewable) {
      $entity_url = !$entity->isNew() ?
        $entity->toUrl('canonical', [
          'query' => [
            'tca' => $token,
          ],
          'absolute' => TRUE,
        ])->toString() :
        $this->t('N/A');

      $form['tca']['tca_active']['#description'] = $this->t('Prevent users from viewing this content without providing an access token via the URL.');
      $form['tca']['tca_active']['#disabled'] = $forced;

      $states = [
        'visible' => [
          ':input[name="tca_active"]' => ['checked' => TRUE],
        ],
      ];

      // Allows content to be viewed without the "View published content"
      // permission.
      $form['tca']['tca_public'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('All users with access token can view this content'),
        '#default_value' => $tca_settings->get('public'),
        '#description' => $this->t('Allow all users to view this content bypassing any permissions restrictions.'),
        '#states' => $states,
      ];

      $form['tca']['tca_token'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Access Token'),
        '#default_value' => $token,
        '#description' => $this->t('Append this access token to the URL as the value for the "tca" query parameter.'),
        '#attributes' => ['readonly' => 'readonly'],
        '#states' => $states,
      ];

      // Attach clipboard functionality.
      $module_handler = $this->moduleHandler;
      if ($module_handler->moduleExists('clipboardjs')) {
        $form['tca']['#attached']['library'][] = 'clipboardjs/clipboardjs';
        $form['tca']['tca_clipboardjs'] = [
          '#type' => 'textfield',
          '#theme' => 'clipboardjs',
          '#text' => $entity_url,
          '#alert_text' => NULL,
          '#disabled' => TRUE,
          '#access' => !empty($token),
          '#states' => $states,
        ];
      }

      $form['tca']['tca_url_copy'] = [
        '#type' => 'item',
        '#title' => $this->t('URL to copy'),
        '#markup' => '<span>' . $entity_url . '</span>',
        '#access' => !empty($token),
        '#states' => $states,
      ];

      $form['tca']['actions'] = [
        '#type' => 'actions',
      ];

      $form['tca']['actions']['tca_regenerate'] = [
        '#type' => 'submit',
        '#value' => $this->t('Generate a new Access Token'),
        '#access' => !empty($token),
        '#ajax' => [
          'callback' => [
            'Drupal\tca\FormManglerService',
            'staticHandleFormAjax',
          ],
        ],
        '#submit' => [
          [
            'Drupal\tca\FormManglerService',
            'staticHandleFormSubmit',
          ],
        ],
        '#states' => $states,
      ];
    }

    // Add "forced" settings to bundle form.
    if ($is_entity_bundle) {
      $states = [
        'visible' => [
          ':input[name="tca_active"]' => ['checked' => TRUE],
        ],
      ];
      // Add force checkbox.
      $form['tca']['tca_force'] = [
        '#type'          => 'checkbox',
        '#title'         => $this->t('Enforce Token usage'),
        '#default_value' => $forced,
        '#description'   => $this->t('Enforce usage of Tokens on this bundle.'),
        '#states'        => $states,
      ];
    }

    // Attach the TCA form to the main form, and add a custom validation
    // callback.
    $attach += $form;

    // Optionally provide a form validation handler.
    $submit_handler_locations = $is_entity_bundle
      ? $entity_plugin->getBundleFormSubmitHandlerAttachLocations()
      : $entity_plugin->getFormSubmitHandlerAttachLocations();

    foreach ($submit_handler_locations as $location) {
      if (is_array($location)) {
        foreach ($location as $subkey) {
          $array_ref = &$array_ref[$subkey];
        }
      }
      else {
        $array_ref = &$array_ref[$location];
      }
      if (isset($array_ref)) {
        $array_ref[] = [
          'Drupal\tca\FormManglerService',
          'staticHandleFormSubmit',
        ];
      }
    }
  }

  /**
   * Handle general aspects of TCA form submission.
   *
   * (Not specific to node etc.).
   *
   * @param array $form
   *   The form.
   * @param string|int|object $form_state
   *   The form state.
   */
  public static function staticHandleFormSubmit(array $form, $form_state) {
    \Drupal::service('tca.form_mangler')
      ->handleFormSubmit($form, $form_state);
  }

  /**
   * Handle general aspects of TCA form submission.
   *
   * (Not specific to node etc.).
   *
   * @param array $form
   *   The form.
   * @param string|int|object $form_state
   *   The form state.
   */
  public function handleFormSubmit(array $form, $form_state) {
    $is_entity_bundle = $form_state->getValue('tca_is_entity_bundle');
    $entity_type_id = $form_state->getValue('tca_entity_type_id');
    $entity_id = $form_state->getValue('tca_entity_id');
    $active = $form_state->getValue('tca_active');
    $forced = !$active ? FALSE : (bool) $form_state->getValue('tca_force');
    $settings = [
      'active' => $active,
      'token' => NULL,
      'public' => $form_state->getValue('tca_public'),
      'force' => $forced,
    ];

    if (!$entity_id) {
      $entity_id = $form_state->getformObject()->getEntity()->id();
      if (!$entity_id) {
        return;
      }
    }

    $triggered_element = $form_state->getTriggeringElement();
    $regenerate = in_array('tca', $triggered_element['#array_parents']);
    $token = NULL;
    $is_new = FALSE;

    // Set token.
    if (!$is_entity_bundle && $active) {
      $tca_settings = $this->tcaSettingsManager
        ->loadSettingsAsConfig($entity_type_id, $entity_id);

      $token = $tca_settings->get('token');
      $is_new = !$token;

      if ($regenerate || $is_new) {
        $token = $this->tcaSettingsManager->generateToken($entity_type_id, $entity_id);
        $form_state->setValue('tca_token', $token);
      }

      $settings['token'] = $token;
    }

    // Doesn't save if Regenearte button was clicked.
    if (!$regenerate) {
      $this->tcaSettingsManager->saveSettings(
        $settings,
        $entity_type_id,
        $entity_id
      );
    }

    // Set message.
    if ($is_new && $token) {
      $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($entity_id);
      $entity_url = $entity->toUrl('canonical', [
        'query' => [
          'tca' => $token,
        ],
        'absolute' => TRUE,
      ])->toString();
      $this->messenger->addMessage($this->t('URL to bypass Token Access Control for this entity: @url', [
        '@url' => $entity_url,
      ]));
    }
  }

  /**
   * Handle general aspects of AJAX form behavior.
   *
   * (Not specific to node etc.).
   *
   * @param array $form
   *   The form.
   * @param string|int|object $form_state
   *   The form state.
   */
  public static function staticHandleFormAjax(array $form, $form_state) {
    $token = $form_state->getValue('tca_token');
    $entity_type_id = $form_state->getValue('tca_entity_type_id');
    $entity_id = $form_state->getValue('tca_entity_id');

    if ($entity_id) {
      $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($entity_id);
      $entity_url = $entity->toUrl('canonical', [
        'query' => [
          'tca' => $token,
        ],
        'absolute' => TRUE,
      ])->toString();
    }
    else {
      $entity_url = t('N/A');
    }

    $response = new AjaxResponse();
    $response->addCommand(new InvokeCommand('[name="tca_token"]', 'val', [$token]));
    $response->addCommand(new InvokeCommand('.form-item-tca-url-copy span', 'text', [$entity_url]));
    $response->addCommand(new InvokeCommand('.js-form-item-tca-clipboardjs .clipboardjs', 'val', [$entity_url]));
    return $response;
  }

  /**
   * Checks if the current entity is a bundle.
   */
  protected function isEntityBundle($entity) {
    return is_subclass_of($entity,
      'Drupal\Core\Config\Entity\ConfigEntityBundleBase');
  }

}
