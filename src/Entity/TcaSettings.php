<?php

namespace Drupal\tca\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\tca\Exception\InvalidTcaSettingException;
use Drupal\tca\TcaSettingsInterface;

/**
 * Defines the TCA settings entity.
 *
 * @ConfigEntityType(
 *   id = "tca_settings",
 *   label = @Translation("Token Content Access settings"),
 *   handlers = {},
 *   config_prefix = "tca_settings",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "active" = "active",
 *     "token" = "token",
 *     "public" = "public"
 *   },
 *   config_export = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "active" = "active",
 *     "token" = "token",
 *     "public" = "public"
 *   },
 *   config_export = {
 *     "id",
 *     "uuid",
 *     "active",
 *     "token",
 *     "public",
 *   },
 *   links = {}
 * )
 */
class TcaSettings extends ConfigEntityBase implements TcaSettingsInterface {

  /**
   * The TCA settings ID.
   *
   * @var string
   */
  protected $id;

  /**
   * Specifies whether or not Token Content Access is active.
   *
   * @var bool
   */
  protected $active;

  /**
   * The TCA token value.
   *
   * @var string
   */
  protected $token;

  /**
   * Indicates if content is public (ignore permissions).
   *
   * @var bool
   */
  protected $public;

  /**
   * The entity type id, eg. 'node_type'.
   *
   * @var string
   */
  protected $entity_type_id;

  /**
   * The entity id, eg. 'article'.
   *
   * @var string
   */
  protected $entity_id;

  /**
   * {@inheritdoc}
   */
  public function setActive($active) {
    if (!is_bool($active)) {
      throw new InvalidTcaSettingException('active');
    }
    $this->active = $active;
  }

  /**
   * {@inheritdoc}
   */
  public function getActive() {
    return $this->active;
  }

  /**
   * {@inheritdoc}
   */
  public function setToken($token) {
    if (!is_null($token) && (!is_string($token) || strlen($token) != 64)) {
      throw new InvalidTcaSettingException('token');
    }
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public function getToken() {
    return $this->token;
  }

  /**
   * {@inheritdoc}
   */
  public function getPublic() {
    return $this->public;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();

    if ($this->entity_type_id && $this->entity_id) {
      // Create dependency on the bundle.
      $entity_type_manager = \Drupal::entityTypeManager();

      $entity = $entity_type_manager->getStorage($this->entity_type_id)
        ->load($this->entity_id);
      $bundle = $entity_type_manager->getDefinition($this->entity_type_id);
      $entity_type = $entity_type_manager->getDefinition(
        $bundle->getBundleOf() ?: $this->entity_type_id);
      $bundle_name = $entity->getEntityType()->getBundleEntityType()
        ? $entity->bundle() : $this->entity_id;

      $bundle_config_dependency = $entity_type->getBundleConfigDependency($bundle_name);
      $this->addDependency($bundle_config_dependency['type'], $bundle_config_dependency['name']);
    }

    return $this;
  }

}
