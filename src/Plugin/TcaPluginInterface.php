<?php

namespace Drupal\tca\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for TCA plugins.
 */
interface TcaPluginInterface extends PluginInspectionInterface {

  /**
   * The fildable mark.
   *
   * Return TRUE if will the settings be stored in fields,
   * otherwise, settings will be stored in config.
   *
   * @return bool
   *   The fildable mark.
   */
  public function isFieldable();

  /**
   * Return locations to attach submit handlers to entities.
   *
   * This should return an array of arrays, e.g.:
   * [
   *   ['actions', 'submit', '#publish'],
   *   ['actions', 'publish', '#submit'],
   * ].
   */
  public function getFormSubmitHandlerAttachLocations();

  /**
   * Return locations to attach submit handlers to entity bundles.
   *
   * This should return an array of arrays, e.g.:
   * [
   *   ['actions', 'submit', '#publish'],
   *   ['actions', 'publish', '#submit'],
   * ].
   *
   * @return array
   *   A multidimensional array.
   */
  public function getBundleFormSubmitHandlerAttachLocations();

  /**
   * Return a map of entity IDs used by this plugin to token IDs.
   *
   * @return array
   *   A map of token IDs to entity IDs in the form
   *   ['entity ID' => 'token ID']
   */
  public function getEntityTokenMap();

}
