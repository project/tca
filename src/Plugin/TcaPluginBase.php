<?php

namespace Drupal\tca\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for TCA plugins.
 */
abstract class TcaPluginBase extends PluginBase implements TcaPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function isFieldable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormSubmitHandlerAttachLocations() {
    return [['actions', 'submit', '#submit']];
  }

  /**
   * {@inheritdoc}
   */
  public function getBundleFormSubmitHandlerAttachLocations() {
    return [['actions', 'submit', '#submit']];
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTokenMap() {
    $map = [];
    $map[$this->pluginDefinition['entityType']]
      = $this->pluginDefinition['entityType'];
    $bundle = \Drupal::entityTypeManager()
      ->getDefinition($this->pluginDefinition['entityType'])
      ->getBundleEntityType();
    if (!empty($bundle)) {
      $map[$bundle] = $bundle;
    }
    return $map;
  }

}
