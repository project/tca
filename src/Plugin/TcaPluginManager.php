<?php

namespace Drupal\tca\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the TCA plugin manager.
 */
class TcaPluginManager extends DefaultPluginManager {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var Drupal\Core\Entity\EntityTypeManager
   */
  private $etm;

  /**
   * Constructs a new \Drupal\tca\Plugin\TcaPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $etm
   *   The entity type manager service.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $etm) {
    parent::__construct('Plugin/TcaPlugin', $namespaces, $module_handler,
      'Drupal\tca\Plugin\TcaPluginInterface', 'Drupal\tca\Annotation\TcaPlugin');
    $this->alterInfo('tca_plugin_info');
    $this->setCacheBackend($cache_backend, 'tca_plugin_plugins');

    $this->etm = $etm;
  }

  /**
   * Create an instance of the first plugin found with string id $entity_type.
   *
   * Create an instance of the first plugin found supporting the entity type
   * with string id $entity_type.
   *
   * @param string $entity_type
   *   The string ID of the entity type.
   *
   * @return Drupal\tca\Plugin\TcaPluginInterface
   *   The plugin.
   */
  public function createInstanceByEntityType($entity_type) {
    $plugin_ids = array_keys($this->loadDefinitionsByEntityType($entity_type));
    return $plugin_ids ? $this->createInstance($plugin_ids[0]) : NULL;
  }

  /**
   * Load plugins implementing entity with id $entity_type.
   *
   * @param string $entity_type
   *   The string ID of the entity type.
   *
   * @return array
   *   An array of plugin definitions for the entity type with ID $entity_type.
   */
  public function loadDefinitionsByEntityType($entity_type) {
    return array_filter($this->getDefinitions(), function ($var) use ($entity_type) {
      return $var['entityType'] == $entity_type;
    });
  }

  /**
   * Load plugins definition by module name.
   *
   * @param string $module_name
   *   The string of module name.
   *
   * @return array
   *   An array of plugin definitions for the entity type with ID $entity_type.
   */
  public function loadDefinitionsByModuleName($module_name) {
    return array_filter($this->getDefinitions(), function ($var) use ($module_name) {
      return $var['provider'] == $module_name;
    });
  }

  /**
   * Load the string IDs for the supported entity types.
   *
   * @return array
   *   An array of entity type ID strings.
   */
  public function loadSupportedEntityTypes() {
    return array_values(array_map(function ($var) {
      return $var['entityType'];
    }, $this->getDefinitions()));
  }

  /**
   * Load the string IDs for the supported bundle entity types.
   *
   * @return array
   *   An array of entity type ID strings.
   */
  public function loadSupportedBundleEntityTypes() {
    return array_values(array_map(function ($var) {
      return $this->etm->getStorage($var['entityType'])
        ->getEntityType()->getBundleEntityType();
    }, $this->getDefinitions()));
  }

  /**
   * Load a map of tokens per entity type.
   *
   * Used for TCA plugins that use tokens like PageRedirect.
   *
   * @return array
   *   An array of token IDs keyed by entity ID
   */
  public function loadEntityTokenMap() {
    $map = [];
    foreach ($this->getDefinitions() as $key => $def) {
      $map += $this->createInstance($key)->getEntityTokenMap();
    }
    return $map;
  }

}
