<?php

namespace Drupal\tca\Exception;

/**
 * Invalid Tca Setting Exception.
 *
 * @package Drupal\tca
 */
class InvalidTcaSettingException extends \Exception {

}
