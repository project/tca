/**
 * @file
 */

(function ($, Drupal) {

  Drupal.behaviors.tca = {
    attach: function (context, settings) {

      $(context).find('.tca-settings-form').each(function () {
        var jThis = $(this);

        // Set clipboard functionality.
        jThis.find('.clipboardjs')
          .css({
            'position': 'absolute',
            'left': '-9999px',
          })
          .parent()
          .css({
            'float': 'right',
          })
          .find('.clipboardjs-button')
          .attr('disabled', 'disabled')
          .data('clipboardAlert', 'none')
          .css({
            'cursor': 'pointer',
          });

        // Display the action in the vertical tab summary.
        jThis.drupalSetSummary(function (context) {
          var $checkbox = $('input:checked', context).length;
          return Drupal.checkPlain($checkbox ? Drupal.t('Enabled') : Drupal.t('Disabled'));
        });
      });

    }
  }

})(jQuery, Drupal);
