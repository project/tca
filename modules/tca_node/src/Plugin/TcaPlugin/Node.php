<?php

namespace Drupal\tca_node\Plugin\TcaPlugin;

use Drupal\tca\Plugin\TcaPluginBase;

/**
 * Implements TCA for nodes.
 *
 * @TcaPlugin(
 *  id = "tca_node",
 *  label = @Translation("Node"),
 *  entityType = "node"
 * )
 */
class Node extends TcaPluginBase {

  /**
   * {@inheritdoc}
   */
  public function isFieldable() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormSubmitHandlerAttachLocations() {
    return [
      ['actions', 'submit', '#submit'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getBundleFormSubmitHandlerAttachLocations() {
    return [
      ['actions', 'submit', '#submit'],
      ['actions', 'save_continue', '#submit'],
    ];
  }

}
