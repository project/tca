<?php

/**
 * @file
 * Contains tca_node.module.
 */

use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Get TCA disabled node types.
 */
function _tca_node_get_disabled_types() {
  $types = \Drupal::entityTypeManager()
    ->getStorage('node_type')
    ->loadMultiple();
  $settings_manager = \Drupal::service('tca.tca_settings_manager');

  $disabled_types = [];
  foreach ($types as $node_type_id => $definition) {
    $settings = $settings_manager->loadSettingsAsConfig('node_type', $node_type_id);
    if (!$settings->get('active')) {
      $disabled_types[] = $node_type_id;
    }
  }

  return $disabled_types;
}

/**
 * Implements hook_query_TAG_alter().
 */
function tca_node_query_search_node_search_alter(AlterableInterface $query) {
  $account = \Drupal::currentUser();
  $bypass_permitted = $account->hasPermission('tca bypass node');

  if (!$bypass_permitted) {
    $group = $query->orConditionGroup()
      ->condition('n.tca_active', NULL, 'IS NULL')
      ->condition('n.tca_active', 0);

    $disabled_types = _tca_node_get_disabled_types();
    if ($disabled_types) {
      $group->condition('n.type', $disabled_types, 'IN');
    }

    $query->condition($group);
  }
}

/**
 * Implements hook_views_query_alter().
 */
function tca_node_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {
  $account = \Drupal::currentUser();
  $bypass_permitted = $account->hasPermission('tca bypass node');

  if (!$bypass_permitted) {
    $disabled_types = _tca_node_get_disabled_types();

    $info = $query->getEntityTableInfo('node_field_data');
    if (isset($info['node'])) {
      $alias = $info['node']['alias'];
      $group_name = 'tca_node';
      $query->where[$group_name] = [
        'conditions' => [
          [
            'field' => $alias . '.tca_active',
            'value' => NULL,
            'operator' => 'IS NULL',
          ],
          [
            'field' => $alias . '.tca_active',
            'value' => 0,
            'operator' => '=',
          ],
        ],
        'args' => [],
        'type' => 'OR',
      ];

      // Add disabled types condition.
      if (!empty($disabled_types)) {
        $query->where[$group_name]['conditions'][] = [
          'field' => $alias . '.type',
          'value' => $disabled_types,
          'operator' => 'IN',
        ];
      }
    }
  }
}
